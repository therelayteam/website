#!/usr/local/bin/python2.7

import cgitb
import cgi
import MySQLdb as mysql
import sys
import json

cgitb.enable()

connection = None

try:
	connection = mysql.connect('localhost', 'therelay_team', 'p0w3rr3lay', 'therelay_db')
	cursor = connection.cursor()
	
	command = """SELECT week_number, team_lead, 
				 assignments, completed_assignments, 
				 carried_assignments 
				 FROM progress_reports
				 ORDER BY week_number DESC"""
	cursor.execute(command)
	data=cursor.fetchall()
	
	
	progress_data = []
	for row in data:
		week = row[0]
		team_lead = row[1]
		assignments= row[2]
		completed = row[3]
		incomplete = row[4]
		progress_data.append({'week': week, 'team_lead': team_lead, 'assignments': assignments, 'completed': completed, 'incomplete': incomplete })
	
	json_data = {'progress': progress_data}
	print "Content-Type: application/json"
	print
	print json.dumps(json_data)
		
except mysql.Error, e:
	print "Error %d: %s" % (e.args[0], e.args[1])
	sys.exit(1)
	
finally:
	if connection:
		connection.close()