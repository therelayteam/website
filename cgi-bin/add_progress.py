#!/usr/local/bin/python2.7

import cgitb
import cgi
import MySQLdb as mysql
import sys

cgitb.enable()

form = cgi.FieldStorage()
team_lead= form['teamLead'].value.replace("'", "''")
assignments = form['assignments'].value.replace("'", "''").replace("\n", "<br/>")
complete = form['complete'].value.replace("'", "''").replace("\n", "<br/>")
incomplete = form['incomplete'].value.replace("'", "''").replace("\n", "<br/>")

print 'Content-type: text/html'
print

connection = None
try:
	connection= mysql.connect('localhost', 'therelay_team', 'p0w3rr3lay', 'therelay_db')
	cursor = connection.cursor()
	
	command = """INSERT INTO progress_reports (team_lead, assignments, completed_assignments, carried_assignments) VALUES('%s', '%s', '%s', '%s') """ %(team_lead, assignments, complete, incomplete)
	cursor.execute(command)
	
		
except mysql.Error, e:
	print "Error %d: %s" % (e.args[0],e.args[1])
	sys.exit(1)
	
finally:
	if connection:
		connection.close()