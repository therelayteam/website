#!/usr/local/bin/python2.7

import cgitb
import cgi
import MySQLdb as mysql
import sys
import json

cgitb.enable()

form = cgi.FieldStorage()
month = form['month'].value.replace("'", "''")
if len(month) == 1:
    month = '0' + month

if int(month) == 12:
	next_month = '01'
else:
	next_month = int(month) + 1
	next_month = next_month.__str__()

if len(next_month) == 1:
	next_month = '0' + next_month
	
connection = None

try:
	connection = mysql.connect('localhost', 'therelay_team', 'p0w3rr3lay', 'therelay_db')
	cursor = connection.cursor()
	
	command = """SELECT date, id, minutes FROM meeting_minutes 
					WHERE date >= '2012-%s-01' AND date <'2012-%s-01'
					ORDER BY date DESC""" % (month, next_month)
	cursor.execute(command)
	data=cursor.fetchall()
	
	
	minutes_data = []
	for row in data:
		date = row[0]
		date= row[0].strftime('%Y-%m-%d')
		id = row[1]
		minutes= row[2]
		minutes_data.append({'date': date, 'id': id, 'data': minutes })
	
	json_data = {'minutes': minutes_data}
	print "Content-Type: application/json"
	print
	print json.dumps(json_data)
		
except mysql.Error, e:
	print "Error %d: %s" % (e.args[0], e.args[1])
	sys.exit(1)
	
finally:
	if connection:
		connection.close()