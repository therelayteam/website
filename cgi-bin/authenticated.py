#!/usr/local/bin/python2.7

import cgitb
import cgi
import MySQLdb as mysql
import sys
import json

cgitb.enable()
try:

	form = cgi.FieldStorage()
	username = form['userID'].value.replace("'", "''")
	sessionID = form['sessionID'].value.replace("'", "''")
except KeyError:
 	print "Content-type: application/json"
	print
	print json.dumps( {'success': False} )
	sys.exit(1)
	
print "Content-type: application/json"
print


connection = None
json_data = {}
try:
	connection = mysql.connect('localhost', 'therelay_team', 'p0w3rr3lay', 'therelay_db')
	cursor = connection.cursor()
	
	command = """SELECT passwd_hash FROM users WHERE username='%s'""" %(username)
	cursor.execute(command)
	data=cursor.fetchone()

	hash = sessionID[:32]
	
	if not hash == data[0]:
		json_data = {'success': False}
	else:
		json_data = {'success': True} 
		
	print json.dumps(json_data)
		
except mysql.Error, e:
	print "Error %d: %s" % (e.args[0],e.args[1])
	sys.exit(1)
	
finally:
	if connection:
		connection.close()