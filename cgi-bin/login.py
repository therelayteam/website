#!/usr/local/bin/python2.7

import cgitb
import cgi
import MySQLdb as mysql
import sys
import hashlib
import json
import time

cgitb.enable()

form = cgi.FieldStorage()
username = form['username'].value.replace("'", "''")
password = form['password'].value.replace("'", "''")
	
print "Content-Type: application/json"


connection = None
json_data = {}
try:
	connection = mysql.connect('localhost', 'therelay_team', 'p0w3rr3lay', 'therelay_db')
	cursor = connection.cursor()
	
	command = """SELECT passwd_hash FROM users WHERE username='%s'""" %(username)
	cursor.execute(command)
	data=cursor.fetchone()
	
	hash_algorithm = hashlib.md5()
	hash_algorithm.update(password)
	hash = hash_algorithm.hexdigest()
	
	if not hash == data[0]:
		json_data = {'success': False}
	else:
		hash_algorithm = hashlib.md5()
		timestamp = time.time()
		hash_algorithm.update(password)
		hash = hash_algorithm.hexdigest()
		print "Set-Cookie: userID=%s; Max-Age=3600; path=/;" % (username)
		print "Set-Cookie: sessionID=%s%d; Max-Age=3600; path=/;" % (hash, timestamp)
		json_data = {'success': True} 
		
	print
	print json.dumps(json_data)
		
except mysql.Error, e:
	print "Error %d: %s" % (e.args[0],e.args[1])
	sys.exit(1)
	
finally:
	if connection:
		connection.close()