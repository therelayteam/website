#!/usr/local/bin/python2.7

import cgitb
import cgi
import MySQLdb as mysql
import sys
import hashlib
import json

cgitb.enable()

form = cgi.FieldStorage()
username = form['username'].value.replace("'", "''")
password = form['new_password'].value.replace("'", "''")
old_password = form['old_password'].value.replace("'", "''")
	
print "Content-Type: application/json"
print

connection = None
json_data = {}
try:
	connection = mysql.connect('localhost', 'therelay_team', 'p0w3rr3lay', 'therelay_db')
	cursor = connection.cursor()
	
	command = """SELECT passwd_hash FROM users WHERE username='%s'""" %(username)
	cursor.execute(command)
	data=cursor.fetchone()
	
	hash_algorithm = hashlib.md5()
	hash_algorithm.update(old_password)
	old_hash = hash_algorithm.hexdigest()
	
	if not old_hash == data[0]:
		json_data = {'success': False, 'reason': 'Old password did not match for user %s' % (username)}
		print json.dumps(json_data)
	else:
		command = """UPDATE users SET passwd_hash=MD5('%s') WHERE username='%s'"""% (password, username)
		cursor.execute(command)
		json_data = {'success': True }
		print json.dumps(json_data)
	
	
	
except mysql.Error, e:
	print "Error %d: %s" % (e.args[0],e.args[1])
	sys.exit(1)
	
finally:
	if connection:
		connection.close()