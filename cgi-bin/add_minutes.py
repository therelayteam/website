#!/usr/local/bin/python2.7

import cgitb
import cgi
import MySQLdb as mysql
import sys

cgitb.enable()

form = cgi.FieldStorage()
date = form['date'].value.replace("'", "''")
minutes = form['minutes'].value.replace("'", "''").replace("\n", "<br/>")

print "Content-type: text/html"
print

connection = None
try:
	connection = mysql.connect('localhost', 'therelay_team', 'p0w3rr3lay', 'therelay_db')
	cursor = connection.cursor()
	
	command = """INSERT INTO meeting_minutes (date, minutes) VALUES('%s', '%s') """ %(date, minutes)
	cursor.execute(command)
	
		
except mysql.Error, e:
	print "Error %d: %s" % (e.args[0],e.args[1])
	sys.exit(1)
	
finally:
	if connection:
		connection.close()