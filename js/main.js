$(function(){
	
	setInterval(getAuthStatus, 1000*60*5)
	
	// Initiallize the tabs
	$('#tabs').tabs({
		show: function(event, ui) {
			$('#calendar').fullCalendar('render');
			resizeContent();
			}
		});
	
	// Set the initial background height
	resizeContent();
	
	// Set the background height to be taller than the tab height when a new tab is selected
	$('#tabs').bind('tabsshow', resizeContent);
	
	
	// Initialize the calendar
	$("#calendar").fullCalendar({
		events: 'https://www.google.com/calendar/feeds/enelprojectdesign%40gmail.com/public/basic',
		editable: true });
		
	// Initialize the login dialog
	$( "#login-form" ).dialog({
			autoOpen: false,
			height: 235,
			width: 350,
			modal: true,
			resizeable: false,
			minHeight: 235,
			buttons: {
				"Login": function() {
				
					$("#error-message").removeClass('ui-state-error');
					$("#error-message").text("");
		
					var usernameVal = $("#username").val();
					var passwordVal = $("#password").val();
					
					if (usernameVal != "" && passwordVal != "")
					{
						$.get("cgi-bin/login.py", {
							username: usernameVal,
							password: passwordVal },
							handleLogin);
					}
				},
				Cancel: function() {
				
					$("#username").val("");
					$("#password").val("");
					$( this ).dialog( "close" );
				}
			}
	});

	$( "#login-link" ).click(function() {
		$("#error-message").removeClass('ui-state-error');
		$("#error-message").text("");
		$( "#login-form" ).dialog( "open" );
	});
	
	$( "#sign-out-link" ).click(function() {
		document.cookie = 'sessionID=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
		document.cookie = 'userID=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
		
		getAuthStatus();
	});
	
	
	$("#datepicker").datepicker({
		dateFormat: "yy-mm-dd"});
	
	$("#submit-meeting-info").click(function() {
	
		$("#datepicker").removeClass("ui-state-error");
		$("#minutes-data").removeClass("ui-state-error");	
			
		var dateVal = $("#datepicker").val();
		var minutesVal = $("#minutes-data").val();
		
		if(dateVal == "" || minutesVal == "")
		{
			$("#datepicker").addClass("ui-state-error");
			$("#minutes-data").addClass("ui-state-error");		
		} 
		else
		{
			$("#datepicker").val("");
			$("#minutes-data").val("");
			$.post("cgi-bin/add_minutes.py", {
				date: dateVal,
				minutes: minutesVal});
		}
		var month = $("#meeting-minutes-month-radio input[name='minutes-radio']:checked").val();
		getMinutes(month);
	});

	var date = new Date();
	getMinutes(date.getMonth());
	setMonthHighlight(date.getMonth());
	getAuthStatus();
	
	$("#meeting-minutes-month-radio").buttonset();
	
	$("#meeting-minutes-month-radio input[name='minutes-radio']").change( function() {
	
		var month = $("#meeting-minutes-month-radio input[name='minutes-radio']:checked").val();
		getMinutes(month);
		
	
	});
	
	$("#submit-progress-info").click(function() {
	
		$("#team-lead-selector").removeClass("ui-state-error");
		$("#assignments-data").removeClass("ui-state-error");
		$("#complete-assignments-data").removeClass("ui-state-error");
		$("#incomplete-assignments-data").removeClass("ui-state-error");
			
		var teamLeadVal = $("#team-lead-selector").val();
		var assignmentVal = $("#assignments-data").val();
		var completeVal = $("#complete-assignments-data").val();
		var incompleteVal = $("#incomplete-assignments-data").val();
		
		if(teamLeadVal == "" || assignmentVal == "" || completeVal =="" || incompleteVal == "" )
		{
			$("#team-lead-selector").addClass("ui-state-error");
			$("#assignments-data").addClass("ui-state-error");
			$("#complete-assignments-data").addClass("ui-state-error");
			$("#incomplete-assignments-data").addClass("ui-state-error");
		} 
		else
		{
			$("#team-lead-selector").val("");
			$("#assignments-data").val("");
			$("#complete-assignments-data").val("");
			$("#incomplete-assignments-data").val("");
			
			$.post("cgi-bin/add_progress.py", {
				teamLead: teamLeadVal,
				assignments: assignmentVal,
				complete: completeVal,
				incomplete: incompleteVal}, displayProgress);
		}
		
		getProgress();
	});
	
	getProgress();
});

function resizeContent() {
	var tabHeight = $("#tabs").height();
	$( "#content" ).animate({height: tabHeight + 30}, "normal", "easeOutExpo");
}

function handleLogin(data) {
	$("#password").val("");
	
	if(data.success)
	{
		$( "#login-form" ).dialog( "close" );
	}
	else
	{
		$("#error-message").addClass('ui-state-error');
		$("#error-message").text("Login Failed");
	}
	
	getAuthStatus();
}

function getCookieValue(cookieName)
{
	var cookieStrings = document.cookie.split(';');
	for (var i = 0; i < cookieStrings.length; i++)
	{
		if (cookieStrings[i] != "")
		{
			var cookieData = cookieStrings[i].split("=");
			var key= cookieData[0].replace(/ /g, '');
			var value= cookieData[1].replace(/ /g, '');
		
			if(key == cookieName)
			{
				return value;
			}
		}
	}
	
	return ""
}

function getAuthStatus()
{
	var session = getCookieValue("sessionID");
	var user = getCookieValue("userID");
	
	$.get("cgi-bin/authenticated.py", {
		sessionID: session,
		userID: user },
		handleAuthAction);
	
}

function handleAuthAction(data)
{
	if(data.success)
	{
		$('#login-link').hide();
		$('.authenticated-only').show();
	}
	else
	{
		$('.authenticated-only').hide();
		$('#login-link').show();
	}
	
	resizeContent();
}

function getMinutes(monthNum)
{
	$.get('cgi-bin/get_minutes.py',{month: monthNum}, displayMinutes);
}

function displayMinutes(data)
{
	$(".meeting-minutes-item").remove();
	
	for(var i = 0; i < data.minutes.length; i++)
	{
		var minutes = data.minutes[i];
		$("#meeting-minutes-content").append('<div id="meeting-minutes'+minutes.id + '" class="meeting-minutes-item"><div class="minutes-title">Date: '+minutes.date+'</div><br/><div class="minutes-data">'+minutes.data+'</div></div>');
	}
	
	resizeContent();
	
}

function setMonthHighlight(month)
{
	var identifier = "#";
	identifier += month;
	identifier += "-minutes-label";
	$(identifier).addClass("ui-state-active");
	
}

function getProgress()
{
	$.get('cgi-bin/get_progress.py', displayProgress);
}

function displayProgress(data)
{
	$(".progress-item").remove();
	for(var i = 0; i< data.progress.length; i++)
	{
		var progress = data.progress[i];
		$("#progress-reports-content").append('<div id="progress-'+progress.week+'" class="progress-item"><div class="progress-data">'+
		'<u><h4>Week ' + progress.week + ', Team Lead - '+progress.team_lead+'</h4></u><h4>Assignments:</h4>'
		+ progress.assignments+'<h4>Complete Tasks:</h4>'+progress.completed+'<h4>Incomplete Tasks:</h4>'
		+ progress.incomplete+'</div></div>');
	}
	
	resizeContent();
}