$(function(){

	$("button#submit-passwd-info").click(function() {
		var oldPass = $('#old-password').val();
		var newPass = $('#new-password').val();
		var newPass2 = $('#new-password2').val();
		var username = $('#username').val();
		
		$('#new-password').removeClass('ui-state-error');
		$('#new-password2').removeClass('ui-state-error');
		$('#error-message').text("");
		$('#error-message').removeClass('ui-state-error');
			
		if (newPass != newPass2)
		{
			$('#new-password').addClass('ui-state-error');
			$('#new-password2').addClass('ui-state-error');
			$('#error-message').text("Please ensure that both new passwords match");
			$('#error-message').addClass('ui-state-error');
		} 
		else if(oldPass != "" && newPass != "" && newPass2 != "" && username != "")
		{
			$.post("cgi-bin/reset_password.py", {
				old_password: oldPass, 
				new_password: newPass, 
				username: username }, handleSubmit);
		}
		
		var oldPass = $('#old-password').val("");
		var newPass = $('#new-password').val("");
		var newPass2 = $('#new-password2').val("");
		
	});
	
	
	$("#popup-dialog").dialog({
		autoOpen:false,
		height: 150,
		minHeight: 150,
		width: 350,
		modal: true,
		resizeable: false,
		textAlign: 'center',
		buttons: {
			"OK": function() {
				window.location = "/"
			},
		},
		close: function() {
			window.location = "/"
		}
	});

});

function handleSubmit(data)
{
	if (data.success == false)
	{
		if(typeof data.reason != 'undefined')
		{
			$('#error-message').text(data.reason);
			$('#error-message').addClass('ui-state-error');
		}	
		else
		{
			$('#error-message').text(data.reason);
			$('#error-message').addClass('ui-state-error');
		}
	}
	else
	{
		$('#popup-dialog').dialog('open');
	}
}
